package UnoGame;

public class AI extends Player {

	/**
	 * Creates new Player object
	 * @param name
	 * @requires name is not null
	 * @ensures the Name of this player will be name 
	 */
	public AI(String name) {
		super(name);
	}
	
	
	
	/**
	 * Places a card 
	 * @param Cards performs the requested play of the player if it is a valid play
	 * @return true if the move is performed successfully 
	 */
	public boolean placeCard() {
		
	//TODO add cards placing 
		return true;
	}
	
	
	
}
