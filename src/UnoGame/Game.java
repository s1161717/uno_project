package UnoGame;

public class Game {

	
	/**
	 * Players of the game
	 * @invariant the minimum length is always 2.
	 * @invariant the length of the array equals the number of players 
	 * @invariant all array items are never null
	 */
	private Player[] players;
	
	/**
	 * Index of current player.
	 * 
	 * @invariant the index is always between 0 and amount of players 
	 * 
	 */
		
	private int playerIndex; 

	//2 player game 
	/** 
	 * Constructor 
	 * 
	 * @param player1 is the first player of the game
	 * @param player2 is the second player of the game
	 */
	public Game(Player player1, Player player2) {
		players = new Player[2];
		players[0]= player1;
		players[1]= player2;
		playerIndex = 0;
		
	}
	
	/**
	 * Start of Uno game
	 * Should ask at end of game if players want to play again. 
	 */
	
	public void start() {
		//TODO needs to be filled
		
	}
	
	/**
	 * Reset the game 
	 * 
	 */
	private void reset() {
		//TODO
	}
	
	
	
}
