package UnoGame;

public class Player {
	//--Instance variables --- 
	private String name =""; //player name
	private int points; // player 
	
	
	/**
	 * Create new Player object
	 * @requires name not null
	 * @ensures the Name of this player will be name 
	 */
	public Player(String name) {
		this.name = name; 
		this.points = 0; //should start as 0
		// TODO figure out card plans
	}
	
	/** 
	 * Returns player name
	 */
	public String getName() {
		return name;
	}
	/**
	 * Returns player points.
	 */
	public int getPoints() {
		return points;
	}
	
	
	
	
}
